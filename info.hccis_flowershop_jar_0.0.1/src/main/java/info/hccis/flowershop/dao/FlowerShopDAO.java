package info.hccis.flowershop.dao;

import info.hccis.flowershop.entity.jpa.FlowerShop;
import info.hccis.flowershop.util.CisUtility;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.IOException;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

/**
 * Class with methods related to FlowerShop Database class
 *
 * @author bjm
 * @since 20200521
 */
public class FlowerShopDAO {

    private String userName=null, password=null, connectionString=null;
    private Connection conn = null;
    private static FileWriter file;
    
    static public void CrunchifyLog(String str) {
        System.out.println("str");
    }
    
    public FlowerShopDAO(){
        String propFileName = "application";
        ResourceBundle rb = ResourceBundle.getBundle(propFileName);
        System.out.println("bjtest datasource:  " + rb.getString("spring.datasource.url"));
        connectionString = rb.getString("spring.datasource.url");
        userName = rb.getString("spring.datasource.username");
        password = rb.getString("spring.datasource.password");

        try {
            conn = DriverManager.getConnection(
                    connectionString, userName, password);
        } catch (SQLException ex) {
            Logger.getLogger(FlowerShopDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    /**
     * Insert a booking into the database.
     * @since 2020-05-21
     * @author BJM
     */
    public void insert(FlowerShop order) {

        //***************************************************
        // INSERT
        //***************************************************
        try {
            String theStatement = "INSERT INTO flowerorder (id, customerID, orderDate, item1, item2, "
                    + "item3, item4, item5, item6, amountPaid, orderStatus, totalCost, createdDateTime) "
                    + "VALUES (0,?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(theStatement);
            stmt.setString(1, order.getCustomerId());
            stmt.setString(2, order.getOrderDate());
            stmt.setString(3, order.getItem1());
            stmt.setString(4, order.getItem2());
            stmt.setString(5, order.getItem3());
            stmt.setString(6, order.getItem4());
            stmt.setString(7, order.getItem5());
            stmt.setString(8, order.getItem6());
            stmt.setString(9, order.getAmountPaid());
            stmt.setString(10, order.getOrderStatus());
            stmt.setString(11, order.getTotalCost());
            stmt.setString(12, CisUtility.getCurrentDate("yyyy-MM-dd hh:mm"));

            stmt.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
        }

    }

    /**
     * Insert a order into the database.
     * @since 2020-05-21
     * @author BJM
     */
    public void update(FlowerShop order) {

        //***************************************************
        // INSERT
        //***************************************************
               
        try {
            String theStatement = "UPDATE booking SET orderDate=?, item1=?, item2=?, item3=?, item4=?, item5=?, item6=?, orderStatus=?, totalCost=?, amountPaid=?, createdDateTime=? WHERE id=?";
            PreparedStatement stmt = conn.prepareStatement(theStatement);
            stmt.setString(1, order.getOrderDate());
            stmt.setString(2, order.getItem1());
            stmt.setString(3, order.getItem2());
            stmt.setString(4, order.getItem3());
            stmt.setString(5, order.getItem4());
            stmt.setString(6, order.getItem5());
            stmt.setString(7, order.getItem6());
            stmt.setString(8, order.getOrderStatus());
            stmt.setString(9, order.getTotalCost());
            stmt.setString(10, order.getAmountPaid());
            stmt.setString(11, CisUtility.getCurrentDate("yyyy-MM-dd hh:mm"));
            stmt.setInt(12, order.getId());

            stmt.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
        }

    }

     /**
     * Delete a order into the database. (Issue#2)
     * @since 2020-06-02
     * @author BJM
     * 
     */
    public void delete(int id) {

        //***************************************************
        // INSERT
        //***************************************************
        try {
            String theStatement = "DELETE FROM flowerorder WHERE id=?";
            PreparedStatement stmt = conn.prepareStatement(theStatement);
            stmt.setInt(1, id);
            stmt.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
        }

    }

    
    
    /**
     * Select all bookings from the database
     * @since 2020-05-21
     * @author BJM
     */
    public ArrayList<FlowerShop> selectAll() {
        ArrayList<FlowerShop> orders = new ArrayList();
        try {
            Statement stmt = conn.createStatement();

            //***************************************************
            // Select using statement
            //***************************************************
            //Next select all the rows and display them here...
            ResultSet rs = stmt.executeQuery("select * from flowerorder");

            //Show all the bookers
            while (rs.next()) {

                int id = rs.getInt("id");
                String customerId = rs.getString("customerId");
                String item1 = rs.getString("item1");
                String item2 = rs.getString("item2");
                String item3 = rs.getString("item3");
                String item4 = rs.getString("item4");
                String item5 = rs.getString("item5");
                String item6 = rs.getString("item6");
                String orderDate = rs.getString("orderDate");
                String orderStatus = rs.getString("orderStatus");
                String totalCost = rs.getString("totalCost");
                String amountPaid = rs.getString("amountPaid");
                FlowerShop order = new FlowerShop(id);
                order.setCustomerId(customerId);
                order.setItem1(item1);
                order.setItem2(item2);
                order.setItem3(item3);
                order.setItem4(item4);
                order.setItem5(item5);
                order.setItem6(item6);
                order.setAmountPaid(amountPaid);
                order.setTotalCost(totalCost);
                order.setOrderDate(orderDate);
                order.setOrderStatus(orderStatus);
                
                // JSON object. Key value pairs are unordered. JSONObject supports java.util.Map interface.
                JSONObject obj = new JSONObject();
                obj.put("Customer ID", rs.getString("customerId"));
                obj.put("Order Date", rs.getString("orderDate"));

                JSONArray flowerOrder = new JSONArray();
                flowerOrder.add("Item 1: " + rs.getString("item1"));
                flowerOrder.add("Item 2: " + rs.getString("item2"));
                flowerOrder.add("Item 3: " + rs.getString("item3"));
                flowerOrder.add("Item 4: " + rs.getString("item4"));
                flowerOrder.add("Item 5: " + rs.getString("item5"));
                flowerOrder.add("Item 6: " + rs.getString("item6"));
                flowerOrder.add("Order Status: " + rs.getString("orderStatus"));
                flowerOrder.add("Total Cost: " + rs.getString("totalCost"));
                flowerOrder.add("Amount Paid: " + rs.getString("amountPaid"));
                obj.put("Flower Order List", flowerOrder);
                try {

                    // Constructs a FileWriter given a file name, using the platform's default charset
                    file = new FileWriter(" c:/fitness/customers_"+ CisUtility.getCurrentDate("yyyy-MM-dd hh:mm") +".json");
                    file.write(obj.toJSONString());
                    CrunchifyLog("Successfully Copied JSON Object to File...");
                    CrunchifyLog("\nJSON Object: " + obj);

                } catch (IOException e) {
                    e.printStackTrace();

                } finally {

                    try {
                        file.flush();
                        file.close();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                
                orders.add(order);
                System.out.println("Order for id: " + rs.getString("id") + " is " + rs.getString("customerId"));
            }
        } catch (SQLException ex) {
            System.out.println("Error selecting orders. (" + ex.getMessage() + ")");

        }
        return orders;

    }

    /**
     * Select a booking from the database
     * @since 2020-05-28
     * @author BJM
     */
    public FlowerShop select(int id) {
        ArrayList<FlowerShop> orders = new ArrayList();
        FlowerShop order = null;

        try {
            Statement stmt = conn.createStatement();

            //***************************************************
            // Select using statement
            //***************************************************
            //Next select all the rows and display them here...
            ResultSet rs = stmt.executeQuery("select * from flowerorder where id="+id);
            
            //Show all the bookers
            while (rs.next()) {

                String customerId = rs.getString("customerId");
                String item1 = rs.getString("item1");
                String item2 = rs.getString("item2");
                String item3 = rs.getString("item3");
                String item4 = rs.getString("item4");
                String item5 = rs.getString("item5");
                String item6 = rs.getString("item6");
                String orderDate = rs.getString("orderDate");
                String orderStatus = rs.getString("orderStatus");
                String totalCost = rs.getString("totalCost");
                String amountPaid = rs.getString("amountPaid");
                order = new FlowerShop(id);
                order.setCustomerId(customerId);
                order.setItem1(item1);
                order.setItem2(item2);
                order.setItem3(item3);
                order.setItem4(item4);
                order.setItem5(item5);
                order.setItem6(item6);
                order.setAmountPaid(amountPaid);
                order.setTotalCost(totalCost);
                order.setOrderDate(orderDate);
                order.setOrderStatus(orderStatus);
                
                orders.add(order);
                System.out.println("Order for id: " + rs.getString("id") + " is " + rs.getString("customerId"));
            }
        } catch (SQLException ex) {
            System.out.println("Error selecting orders. (" + ex.getMessage() + ")");
        }
        return order;

    }


}
