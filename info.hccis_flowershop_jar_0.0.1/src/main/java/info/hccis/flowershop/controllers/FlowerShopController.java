package info.hccis.flowershop.controllers;

import info.hccis.flowershop.entity.jpa.FlowerShop;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import info.hccis.flowershop.repositories.FlowerShopRepository;

/**
 * Controller for the member functionality of the site
 *
 * @since 20191212
 * @author Fred Campos
 */
@Controller
@RequestMapping("/orders")
public class FlowerShopController {

    private final FlowerShopRepository orderRepository;

    public FlowerShopController(FlowerShopRepository br) {
        orderRepository = br;
    }

    /**
     * Page to allow user to view member bookings
     *
     * @since 20200528
     * @author BJM (modified from Fred/Amro's project
     */
    @RequestMapping("/list")
    public String list(Model model) {

        //Go get the bookings from the database.
//        BookingDAO bookingDAO = new BookingDAO();
//        ArrayList<Booking> bookings = bookingDAO.selectAll();
        ArrayList<FlowerShop> orders = (ArrayList<FlowerShop>) orderRepository.findAll();
        model.addAttribute("orders", orders);

        return "orders/list";
    }

    /**
     * Page to allow user to view member bookings
     *
     * @since 20200528
     * @author BJM (modified from Fred/Amro's project
     */
    @RequestMapping("/add")
    public String add(Model model) {

        FlowerShop order = new FlowerShop();
        order.setId(0);
        model.addAttribute("order", order);

        return "orders/add";
    }

    /**
     * Page to allow user to find member bookings
     *
     * @since 20200604
     * @author BJM (modified from Fred/Amro's project
     */
    @RequestMapping("/find")
    public String find(Model model) {

        //Load the names into the model
        Set<String> names = new HashSet();
        
        ArrayList<FlowerShop> orders = (ArrayList<FlowerShop>) orderRepository.findAll();
        for(FlowerShop current: orders){
            names.add(current.getCustomerId());
        }
                
        model.addAttribute("names", names);
        
        return "orders/find";
    }

    /**
     * Page to allow user to find a member bookings
     *
     * @since 20200604
     * @author BJM (modified from Fred/Amro's project
     */
    @RequestMapping("/edit")
    public String edit(Model model, HttpServletRequest request) {

        String idString = (String) request.getParameter("id");
        int id = Integer.parseInt(idString);
        System.out.println("BJTEST - id=" + id);

//        BookingDAO bookingDAO = new BookingDAO();
        Optional<FlowerShop> selectedOrder = orderRepository.findById(id);

//        ArrayList<FlowerShop> nidhiyaOrders = orderRepository.findAllByCustomerId("Nidhiya Nair");
//        System.out.println("BJTEST Nidhiya orders: ");
//        for (FlowerShop current : nidhiyaOrders) {
//            System.out.println(current.getOrderDate());
//        }

        if (selectedOrder == null) {
            return "index";
        } else {
            model.addAttribute("order", selectedOrder);
            return "orders/add";
        }
    }

    /**
     * Page to allow user to delete member bookings
     *
     * @since 20200602
     * @author BJM (modified from Fred/Amro's project
     */
    @RequestMapping("/delete")
    public String delete(Model model, HttpServletRequest request) {

        String idString = (String) request.getParameter("id");
        int id = Integer.parseInt(idString);
        System.out.println("BJTEST - id=" + id);

//        BookingDAO bookingDAO = new BookingDAO();
//        bookingDAO.delete(id);
        orderRepository.deleteById(id);

        ArrayList<FlowerShop> orders = (ArrayList<FlowerShop>) orderRepository.findAll();
        model.addAttribute("orders", orders);

        //send the user to the list page.
        return "orders/list";

    }

    /**
     * Page to allow user to view member bookings
     *
     * @since 20200528
     * @author BJM (modified from Fred/Amro's project
     */
    @RequestMapping("/addSubmit")
    public String addSubmit(Model model, @Valid @ModelAttribute("order") FlowerShop order, BindingResult result) {

        //If errors put the object back in model and send back to the add page.
        if (result.hasErrors()) {
            System.out.println("Validation error");
            return "orders/add";
        }

//        //Apply validation logic.
//        if(booking.getName1().isEmpty()){
//            String message = "name 1 is needed.";
//            model.addAttribute("message", message);
//            model.addAttribute("booking",booking);
//            return "bookings/add";
//        }
//        BookingBO.addBooking(booking);
        orderRepository.save(order);

        //reload the list of bookings
//        BookingDAO bookingDAO = new BookingDAO();
        ArrayList<FlowerShop> orders = (ArrayList<FlowerShop>) orderRepository.findAll();
        model.addAttribute("orders", orders);

        //send the user to the list page.
        return "orders/list";
    }

     /**
     * Page to allow user to view a member bookings
     *
     * @since 20200604
     * @author BJM (modified from Fred/Amro's project
     */
    @RequestMapping("/findSubmit")
    public String findSubmit(Model model, @ModelAttribute("order") FlowerShop order) {

        System.out.println("BJTEST: about to find "+order.getCustomerId()+"'s orders");
        ArrayList<FlowerShop> orders = (ArrayList<FlowerShop>) orderRepository.findAllByCustomerId(order.getCustomerId());
        model.addAttribute("orders", orders);

        //Put the name in the model as well so it can be shown on the list view
        model.addAttribute("findNameMessage", " ("+order.getCustomerId()+")");

        
        //send the user to the list page.
        return "orders/list";
    }

    
    
}
