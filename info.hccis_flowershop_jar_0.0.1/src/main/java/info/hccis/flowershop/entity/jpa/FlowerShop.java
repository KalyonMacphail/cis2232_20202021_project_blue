/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.flowershop.entity.jpa;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author bjmac
 */
@Entity
@Table(name = "order")
@NamedQueries({
    @NamedQuery(name = "FlowerShop.findAll", query = "SELECT b FROM FlowerShop b")})
public class FlowerShop implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "customerId")
    private String customerId;
    @Size(max = 10)
    @Column(name = "orderDate")
    private String orderDate;
    @Size(max = 100)
    @Column(name = "item1")
    private String item1;
    @Size(max = 100)
    @Column(name = "item2")
    private String item2;
    @Size(max = 100)
    @Column(name = "item3")
    private String item3;
    @Size(max = 100)
    @Column(name = "item4")
    private String item4;
    @Size(max = 100)
    @Column(name = "item5")
    private String item5;
    @Size(max = 100)
    @Column(name = "item6")
    private String item6;
    @Size(max = 100)
    @Column(name = "orderStatus")
    private String orderStatus;
    @Size(max = 100)
    @Column(name = "totalCost")
    private String totalCost;
    @Size(max = 100)
    @Column(name = "amountPaid")
    private String amountPaid;

    public FlowerShop() {
    }

    public FlowerShop(Integer id) {
        this.id = id;
    }

    public FlowerShop(Integer id, String customerId) {
        this.id = id;
        this.customerId = customerId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getItem1() {
        return item1;
    }

    public void setItem1(String item1) {
        this.item1 = item1;
    }

    public String getItem2() {
        return item2;
    }

    public void setItem2(String item2) {
        this.item2 = item2;
    }

    public String getItem3() {
        return item3;
    }

    public void setItem3(String item3) {
        this.item3 = item3;
    }

    public String getItem4() {
        return item4;
    }

    public void setItem4(String item4) {
        this.item4 = item4;
    }

    public String getItem5() {
        return item5;
    }

    public void setItem5(String item5) {
        this.item5 = item5;
    }

    public String getItem6() {
        return item6;
    }

    public void setItem6(String item6) {
        this.item6 = item6;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(String totalCost) {
        this.totalCost = totalCost;
    }

    public String getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(String amountPaid) {
        this.amountPaid = amountPaid;
    }

    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FlowerShop)) {
            return false;
        }
        FlowerShop other = (FlowerShop) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.flowershop.entity.jpa.FlowerShop[ id=" + id + " ]";
    }
    
}
