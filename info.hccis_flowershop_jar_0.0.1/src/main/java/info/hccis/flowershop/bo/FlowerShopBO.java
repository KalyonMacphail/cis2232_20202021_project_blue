package info.hccis.flowershop.bo;

import info.hccis.flowershop.dao.FlowerShopDAO;
import info.hccis.flowershop.entity.jpa.FlowerShop;

/**
 *
 *
 * @author bjm
 * @since 2020-05-28
 */
public class FlowerShopBO {

    public static void addOrder(FlowerShop order) {

        System.out.println("Order object about to be added to the database"
                + ""
                + "\n" + order.toString());

        //Add that booking to the database
        FlowerShopDAO orderDAO = new FlowerShopDAO();

        if (order.getId() == 0) {
            orderDAO.insert(order);
        } else {
            orderDAO.update(order);
        }

    }

}
