package info.hccis.flowershop.repositories;

import info.hccis.flowershop.entity.jpa.FlowerShop;
import java.util.ArrayList;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FlowerShopRepository extends CrudRepository<FlowerShop, Integer> {
        ArrayList<FlowerShop> findAllByCustomerId(String customerID);
}